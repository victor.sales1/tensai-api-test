import './App.css';

import { useState, useEffect } from 'react'
import * as Styles from './App.styles'

import { SurveyFactory } from "@sogo_tensai-ecosystem/tensai-forms-client";

function App() {
  
  const [modalState,setModalState] = useState({
    feedback: '',
    showFeedback: {
      show: false,
      type: ''
    }
  })
  useEffect(() => {
    SurveyFactory.createInstance(
      "eyJhbGciOiJIUzI1NiJ9.eyJzdWIiOiItMiIsImlkX3Blc3F1aXNhIjo2LCJpZF92aW5jdWxvX29wZXJhZG9yIjo2LCJzdWJfdGVuYW50IjoidGVuc2FpIiwiaWF0IjoxNjI0Mjk4NTI2LCJleHAiOjMzMTgxMTUzMjAwfQ.Oxy0EBtItkaS4p1PeCnterDFclAr91NSxRxvhbAkzh4",
      "main"
    ).then((value) => {
      value.execute(() => {
        setModalState({
          feedback: 'Deu booom! Ihuuu!!',
          showFeedback: {
            show: true,
            type: 'success'
          },
        })
        console.log("deu bom");
        value.end();
      },
      (s, e) => { 
        console.log("deu ruim", e);
        setModalState({
          feedback: 'Ihhh! Deu ruim!!',
          showFeedback: {
            show: true,
            type: 'error'
          },
        })
      });
    });
  }, [])

  return (
    <Styles.Wrapper>
      <Styles.Title>Tensai-api test</Styles.Title>
      {modalState?.showFeedback?.show && <Styles.ModalOverlay>
        <Styles.ModalContent
          error={modalState?.showFeedback?.type === 'error'}
          onClick={() => setModalState({
            feedback: '',
            showFeedback: {
            show: false,
            type: ''
          }})}>
          {modalState.feedback}
        </Styles.ModalContent>
      </Styles.ModalOverlay>}
      <Styles.Content id="main" />
        {/*here we insert the code from tensai-api*/}
    </Styles.Wrapper>
  );
}

export default App;
