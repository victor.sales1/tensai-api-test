import styled from 'styled-components'

export const Wrapper = styled.div`
  display: flex;
  flex-direction: column;
  width: 100%;
  height: 100vh;
  position: relative;
`

export const Title = styled.h2`
  text-align: center;
`

export const Content = styled.div`
  display: flex;
  height: 50%;
`

export const ModalOverlay = styled.div`
  position: absolute;
  top: 0;
  left: 0;
  right: 0;
  bottom: 0;
  content: '';
  background-color: #00000066;
  display: flex;
  justify-content: center;
  align-items: center;
`

export const ModalContent = styled.div`
  display: flex;
  box-sizing: border-box;
  padding: 20px 60px;
  font-family: -apple-system, BlinkMacSystemFont, 'Segoe UI', Roboto, Oxygen, Ubuntu, Cantarell, 'Open Sans', 'Helvetica Neue', sans-serif;
  font-size: 16px;
  color: #fff;
  font-weight: bold;
  background-color: ${props => props.error ? 'tomato' : 'lightgreen'};
  border-radius: 15px;
  cursor: pointer;
`